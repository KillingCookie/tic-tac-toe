package dev.tictactoe.gfx;

import java.awt.image.BufferedImage;

public class Sprite {

	private BufferedImage Sheet;
	
	public Sprite(BufferedImage Sheet){
		
		this.Sheet = Sheet;
		
	}
	
	public BufferedImage Crop(int x, int y, int width, int height){
		
		return Sheet.getSubimage(x, y, width, height);
		
	}
	
}
