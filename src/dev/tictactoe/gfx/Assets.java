package dev.tictactoe.gfx;

import java.awt.image.BufferedImage;

public class Assets {

	private static final int width = 128, height = 128;
	
	public static BufferedImage Null, X, O, cross1, cross2, cross3, cross4, Xturn, Oturn, Owon, Xwon, restart, menu, draw,
								mainMenu, OptionsMenu, Play, oneplayer,
								PLAY, OPTIONS;
	
	public static void init(){
		
		mainMenu = TextureLoader.LoadImg("/textures/MainMenu.png");
		OptionsMenu = TextureLoader.LoadImg("/textures/Options.png");
		Play = TextureLoader.LoadImg("/textures/Play.png");
		oneplayer = TextureLoader.LoadImg("/textures/1player.png");
		
		Sprite buttons = new Sprite(TextureLoader.LoadImg("/textures/Buttons.png"));
		
		PLAY = buttons.Crop(0, 0, 51, 28);
		OPTIONS = buttons.Crop(0, 28, 97, 27);
	}
	
	public static void gameinit(boolean whichone){
		
		Sprite sheet = new Sprite(TextureLoader.LoadImg("/textures/AllSheet.png"));
		
		if(whichone){
			
			O = sheet.Crop(width*3, 0, width, height);
			X = sheet.Crop(width*3, height, width, height);
		}else{
			
			O = sheet.Crop(width, 0, width, height);
			X = sheet.Crop(width*2, 0, width, height);
		}
		
		Null = sheet.Crop(0, 0, width, height);
		cross1 = sheet.Crop(0, height, width, height);
		cross2 = sheet.Crop(width, height, width, height);
		cross3 = sheet.Crop(width*2, height, width, height);
		cross4 = sheet.Crop(width, height*2, width, height);
		Xturn = sheet.Crop(0, height*2, width, 26);
		Oturn = sheet.Crop(width*2, height*2, width, 26);
		Owon = sheet.Crop(0, height*2 + 26, width, 26);
		Xwon = sheet.Crop(width*2, height*2 + 26, width, 26);
		restart = sheet.Crop(0, height*2 + 26*2, width, 26);
		menu = sheet.Crop(0, height*2 + 26*3, width, 26);
		draw = sheet.Crop(width*2, height*2 + 2*26, width, 26);
			
	}
		
}
