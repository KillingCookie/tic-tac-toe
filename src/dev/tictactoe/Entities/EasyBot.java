package dev.tictactoe.Entities;

import java.util.concurrent.ThreadLocalRandom;

import dev.tictactoe.Game;

public class EasyBot extends Bot {
	
	private int num;
	
	public int[] arr = {1,1,1,1,1,1,1,1,1,};

	public EasyBot(Game game) {
		
		super(game);
		
	}

	@Override
	public int turn(int[][] a) {
		
		num = ThreadLocalRandom.current().nextInt(2,20);
		
		if(a[0][0] != 0 && num <= 3) {
			turn(a);
		}else if(a[1][0] != 0 && num > 3 && num <= 5) {
			turn(a);
		}else if(a[2][0] != 0 && num > 5 && num <= 7) {
			turn(a);
		}else if(a[0][1] != 0 && num > 7 && num <= 9) {
			turn(a);
		}else if(a[1][1] != 0 && num > 9 && num <= 11) {
			turn(a);
		}else if(a[2][1] != 0 && num > 11 && num <= 13) {
			turn(a);
		}else if(a[0][2] != 0 && num > 13 && num <= 15) {
			turn(a);
		}else if(a[1][2] != 0 && num > 15 && num <= 17) {
			turn(a);
		}else if(a[2][2] != 0 && num > 17 && num <= 19) 
			turn(a);
		
		return num/2;
		
	}
	
}
