package dev.tictactoe.Entities;

import dev.tictactoe.Game;

public abstract class Bot {
	
	private static Bot currentBot = null;
	
	public static void setBot(Bot bot) {
		
		currentBot = bot;
		
	}
	
	public static Bot getBot() {
		
		return currentBot;
		
	}
	
	protected Game game;
	
	public Bot(Game game) {
		
		this.game = game;
		
	}
	
	public abstract int turn(int[][] a);

}
