package dev.tictactoe;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import dev.tictactoe.Entities.Bot;
import dev.tictactoe.Entities.EasyBot;
import dev.tictactoe.Entities.HardBot;
import dev.tictactoe.Frame.Frame;
import dev.tictactoe.Input.Keyboard;
import dev.tictactoe.Input.Mouse;
import dev.tictactoe.States.GameState;
import dev.tictactoe.States.MenuState;
import dev.tictactoe.States.OnePlayerState;
import dev.tictactoe.States.OptionsState;
import dev.tictactoe.States.PlayState;
import dev.tictactoe.States.State;
import dev.tictactoe.gfx.Assets;

public class Game implements Runnable {

	private Frame gui;
	
	private Thread thread;
	
	private boolean Run = false;
	private int frame_width, frame_height;
	private String frame_title;
	
	private BufferStrategy BufStr;
	private Graphics G;
	
	private Keyboard keyboard;
	private Mouse mouse;
	
	public State gameState;
	public State menuState;
	public State optionsState;
	public State playState;
	public State onePlayerState;
	
	private boolean whichone = true;
	
	public Bot easyBot;
	public Bot hardBot;
	
	public Game(String frame_title, int frame_width, int frame_height){
		
		this.frame_width = frame_width;
		this.frame_height = frame_height;
		this.frame_title = frame_title;
		keyboard = new Keyboard(this);
		mouse = new Mouse();
	}
	
	private void init(){
		
		gui = new Frame(frame_title, frame_width, frame_height);
		
		gui.getFrame().addKeyListener(keyboard);
		gui.getFrame().addMouseListener(mouse);
		gui.getFrame().addMouseMotionListener(mouse);
		gui.getCanvas().addMouseListener(mouse);
		gui.getCanvas().addMouseMotionListener(mouse);
		
		Assets.init();
		
		gameState = new GameState(this);
		menuState = new MenuState(this);
		playState = new PlayState(this);
		optionsState = new OptionsState(this);
		onePlayerState = new OnePlayerState(this);
		State.setState(menuState);
		
		easyBot = new EasyBot(this);
		hardBot = new HardBot(this);
		Bot.setBot(easyBot);
		
	}

	private void tick(){
		
		keyboard.tick();
		
		if(State.getState() != null){
			
			State.getState().tick();
			
		}
		
	}
	
	private void render(){
		
		BufStr = gui.getCanvas().getBufferStrategy();
		if(BufStr == null){
			gui.getCanvas().createBufferStrategy(3);
			return;
		}
		
		G = BufStr.getDrawGraphics();
		
		G.clearRect(0, 0, frame_width, frame_height); //clear screen
		//
		
		if(State.getState() != null){
			
			State.getState().render(G);
			
		}
		
		//
		BufStr.show();
		G.dispose();
		
	}
	
	public void run(){
		
		init();
		
		int tps = 60, fps = 0;
		double timePerTick = 1000000000 / tps, delta = 0;
		long now, lastTime = System.nanoTime();
		
		while(Run){
			
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			lastTime = now;
			
			if(delta >= 1){
				
				tick();
				System.out.println(fps*tps);
				fps = 0;
				delta--;
				
			}render();
			fps++;
		
		} 
		
		stop();
		
	}
	
	public Keyboard getKeyboard(){
		
		return keyboard;
		
	}
	
	public Mouse getMouse() {
		
		return mouse;
		
	}
	
	public boolean getWhichone(){
		
		return whichone;
	}
	
	public void setWhichone(boolean a){
		
		whichone = a;
	}
	
	public synchronized void start(){
		
		if(Run){
			return;
		}else{
			Run = true;
			thread = new Thread(this);
			thread.start();
			
		}
		
	}

	public synchronized void stop(){
		
		if(!Run){
			return;
		}else{
			Run = false;
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
