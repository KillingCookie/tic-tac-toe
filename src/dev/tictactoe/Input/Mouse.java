package dev.tictactoe.Input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Mouse implements MouseListener, MouseMotionListener {
	
	private boolean leftPressed, rightPressed;
	private int xPosition, yPosition;
	
	public Mouse(){
		
		
		
	}
	
	//getters
	
	public boolean isLeftPressed() {
		
		return leftPressed;
		
	}
	
	public void setLeft(boolean a) {
		
		leftPressed = a;
		
	}
	
	public boolean isRightPressed() {
		
		return rightPressed;
		
	}
	
	public int getX() {
		
		return xPosition;
		
	}
	
	public int getY() {
		
		return yPosition;
		
	}
	
	//implimented methods
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		if(e.getButton() == MouseEvent.BUTTON1)
			leftPressed = true;
		
		if(e.getButton() == MouseEvent.BUTTON2)
			rightPressed = true;
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		if(e.getButton() == MouseEvent.BUTTON1)
			leftPressed = false;
		
		if(e.getButton() == MouseEvent.BUTTON2)
			rightPressed = false;
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
		xPosition = e.getX();
		yPosition = e.getY();
		
	}
	
}
