package dev.tictactoe.Input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import dev.tictactoe.Game;
import dev.tictactoe.States.State;

public class Keyboard implements KeyListener {
	
	private Game game;
	
	private boolean[] keys;
	public boolean upleft, up, upright, left, centre, right, downleft, down, downright, restart, backToMenu,
					ent,
					upp, downn;
	
	public Keyboard(Game game){
		
		keys = new boolean[256];
		
		this.game = game;
	}
	
	public void tick(){
		
		if(State.getState() == game.menuState){
			
			upp = keys[KeyEvent.VK_UP];
			downn = keys[KeyEvent.VK_DOWN];
			ent = keys[KeyEvent.VK_ENTER];
		}else if(State.getState() == game.gameState){
		
			upleft = keys[KeyEvent.VK_NUMPAD7];
			up = keys[KeyEvent.VK_NUMPAD8];
			upright = keys[KeyEvent.VK_NUMPAD9];
			left = keys[KeyEvent.VK_NUMPAD4];
			centre = keys[KeyEvent.VK_NUMPAD5];
			right = keys[KeyEvent.VK_NUMPAD6];
			downleft = keys[KeyEvent.VK_NUMPAD1];
			down = keys[KeyEvent.VK_NUMPAD2];
			downright = keys[KeyEvent.VK_NUMPAD3];
			restart = keys[KeyEvent.VK_R];
			backToMenu = keys[KeyEvent.VK_ESCAPE];
		}else if(State.getState() == game.optionsState){
			
			upp = keys[KeyEvent.VK_UP];
			downn = keys[KeyEvent.VK_DOWN];
			ent = keys[KeyEvent.VK_ENTER];
			backToMenu = keys[KeyEvent.VK_ESCAPE];
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		
		keys[e.getKeyCode()] = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		keys[e.getKeyCode()] = false;
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		
		
	}

}
