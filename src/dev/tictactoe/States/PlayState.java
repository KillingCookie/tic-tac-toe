package dev.tictactoe.States;

import java.awt.Graphics;

import dev.tictactoe.Game;
import dev.tictactoe.Entities.Bot;
import dev.tictactoe.gfx.Assets;

public class PlayState extends State {

	public PlayState(Game game) {
		
		super(game);
		
	}

	@Override
	public void tick() {
		
		if(game.getMouse().getX() > 92 && game.getMouse().getX() < 384 - 92 && game.getMouse().getY() > 55 && game.getMouse().getY() < 110 && game.getMouse().isLeftPressed()) {
			
			game.getMouse().setLeft(false);
			Bot.setBot(null);
			Assets.gameinit(game.getWhichone());
			State.setState(game.gameState);
			
		}else if(game.getMouse().getX() > 92 && game.getMouse().getX() < 384 - 92 && game.getMouse().getY() > 165 && game.getMouse().getY() < 220 && game.getMouse().isLeftPressed()) {
			
			game.getMouse().setLeft(false);
			State.setState(game.onePlayerState);
			
		}else if(game.getMouse().getX() > 384 - 16 && game.getMouse().getY() < 16 && game.getMouse().isLeftPressed()){
			
			game.getMouse().setLeft(false);
			State.setState(game.menuState);
			
		}
		
	}

	@Override
	public void render(Graphics g) {
		
		g.drawImage(Assets.Play, 0, 0, null);
		
	}

}
