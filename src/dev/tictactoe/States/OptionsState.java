package dev.tictactoe.States;

import java.awt.Color;
import java.awt.Graphics;

import dev.tictactoe.Game;
import dev.tictactoe.gfx.Assets;

public class OptionsState extends State {
	
	private int SKIN = 3;

	public OptionsState(Game game){
		
		super(game);
		
	}
	
	@Override
	public void tick() {
		
		if(game.getKeyboard().backToMenu) {
			
			State.setState(game.menuState);
			
		}
			
		if(game.getMouse().getX() > 64 && game.getMouse().getX() < 384 - 64 && game.getMouse().getY() < 166 && game.getMouse().getY() > 39){
				
			SKIN = 1;
			
			if(game.getMouse().isLeftPressed()) {
			
				game.setWhichone(true);
			}
				
		}else if(game.getMouse().getX() > 64 && game.getMouse().getX() < 384 - 64 && game.getMouse().getY() > 166 + 78 && game.getMouse().getY() < 410 - 40){
			
			SKIN = 2;
			
			if(game.getMouse().isLeftPressed()) {
			
				game.setWhichone(false);
			}
				
		}else if(game.getMouse().getX() > 384 - 16 && game.getMouse().getY() < 16 && game.getMouse().isLeftPressed()){
			
			game.getMouse().setLeft(false);
			State.setState(game.menuState);
			
		}else
			SKIN = 3;
			
	}

	@Override
	public void render(Graphics G) {
		
		G.drawImage(Assets.OptionsMenu, 0, 0, null);
		
		switch(SKIN) {
		
		case 1:
			
			G.setColor(Color.red);
			G.drawRect(63, 37, 257, 129);
			G.drawRect(67, 41, 249, 121);
			break;
		case 2:
			
			G.setColor(Color.red);
			G.drawRect(63, 243, 257, 129);
			G.drawRect(67, 247, 249, 121);
			break;
		}
		
		if(game.getWhichone()){
			
			G.setColor(Color.green);
			G.drawRect(64, 38, 255, 127);
			G.drawRect(65, 39, 253, 125);
			G.drawRect(66, 40, 251, 123);
		}else{
			
			G.setColor(Color.green);
			G.drawRect(64, 244, 255, 127);
			G.drawRect(65, 245, 253, 125);
			G.drawRect(66, 246, 251, 123);
		}
		
	}
	
}
