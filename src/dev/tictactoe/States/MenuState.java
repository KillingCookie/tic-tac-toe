package dev.tictactoe.States;

import java.awt.Graphics;

import dev.tictactoe.Game;
import dev.tictactoe.gfx.Assets;

public class MenuState extends State {
	
	private int button = 3;
	
	public MenuState(Game game){
		
		super(game);
		
	}
	
	@Override
	public void tick() {
		
		if((game.getMouse().getX() > 107 && game.getMouse().getX() < (384 - 107)) && (game.getMouse().getY() > 120 && game.getMouse().getY() < 201)){
			
			button = 1;
			
			if(game.getMouse().isLeftPressed()){
				
				game.getMouse().setLeft(false);
				State.setState(game.playState);
			}
		
		}else if((game.getMouse().getX() > 107 && game.getMouse().getX() < (384 - 107)) && (game.getMouse().getY() > 281 && game.getMouse().getY() < 362)){
			
			button = 2;
			if(game.getMouse().isLeftPressed()){
				
				game.getMouse().setLeft(false);
				State.setState(game.optionsState);
				
			}
			
		}else
			button = 3;
			
	}
		

	@Override
	public void render(Graphics G) {
		
		G.drawImage(Assets.mainMenu, 0, 0, null);
		
		switch(button) {
			
			case 1: G.drawImage(Assets.PLAY, 167, 146, null);
				break;
			
			case 2: G.drawImage(Assets.OPTIONS, 143, 307, null);
				break;
				
		}
		
	}
	
}
