package dev.tictactoe.States;

import java.awt.Graphics;

import dev.tictactoe.Game;
import dev.tictactoe.Entities.Bot;
import dev.tictactoe.gfx.Assets;

public class GameState extends State {
	
	private int tileX = 128, tileY = 128;
	private int checkField = 0;
	private int[][] field = new int[3][3];
	private boolean turn = true, win = false, full = false;

	public GameState(Game game){
		
		super(game);
		
		for(int i = 0; i <= 2; i++){
			for(int j = 0; j <= 2; j++){
				field[i][j] = 0;
			}
			
		}
		
	}
	
	@Override
	public void tick() {
		
		if(win){
			
			if(game.getKeyboard().backToMenu || (game.getMouse().isLeftPressed() && game.getMouse().getX() < tileX*3 && game.getMouse().getX() > tileX*2 && game.getMouse().getY() < 26)){
				
				for(int i = 0; i <= 2; i++){
					for(int j = 0; j <= 2; j++){
						field[i][j] = 0;
					}
					
				}
				
				turn = true;
				win = false;
				State.setState(game.menuState);
			}
			
			if(game.getKeyboard().restart || (game.getMouse().isLeftPressed() && game.getMouse().getX() > tileX && game.getMouse().getX() < 2*tileX && game.getMouse().getY() < 26)){
				

				for(int i = 0; i <= 2; i++){
					for(int j = 0; j <= 2; j++){
						field[i][j] = 0;
					}
					
				}
				
				turn = true;
				win = false;
			}
			
		}
		
		if(!win){
		
			if(turn){
		
				if((game.getKeyboard().upleft || (game.getMouse().isLeftPressed() && (game.getMouse().getX() < tileX && game.getMouse().getY() < tileY + 26 && game.getMouse().getY() > 26))) && field[0][0] == 0){
			
					field[0][0] = 1;
					turn = false;
				}else if((game.getKeyboard().up || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 2*tileX && game.getMouse().getX() > tileX) && game.getMouse().getY() < tileY + 26 && game.getMouse().getY() > 26))) && field[1][0] == 0){
				
					field[1][0] = 1;
					turn = false;
				}else if((game.getKeyboard().upright || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 3*tileX && game.getMouse().getX() > 2*tileX) && game.getMouse().getY() < tileY + 26 && game.getMouse().getY() > 26))) && field[2][0] == 0){
				
					field[2][0] = 1;
					turn = false;
				}else if((game.getKeyboard().left || (game.getMouse().isLeftPressed() && (game.getMouse().getX() < tileX && (game.getMouse().getY() < 2*tileY + 26 && game.getMouse().getY() > tileY + 26)))) && field[0][1] == 0){
				
					field[0][1] = 1;
					turn = false;
				}else if((game.getKeyboard().centre || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 2*tileX && game.getMouse().getX() > tileX) && (game.getMouse().getY() < 2*tileY + 26 && game.getMouse().getY() > tileY + 26)))) && field[1][1] == 0){
				
					field[1][1] = 1;
					turn = false;
				}else if((game.getKeyboard().right || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 3*tileX && game.getMouse().getX() > 2*tileX) && (game.getMouse().getY() < 2*tileY + 26 && game.getMouse().getY() > tileY + 26)))) && field[2][1] == 0){
				
					field[2][1] = 1;
					turn = false;
				}else if((game.getKeyboard().downleft || (game.getMouse().isLeftPressed() && (game.getMouse().getX() < tileX && (game.getMouse().getY() < 3*tileY + 26 && game.getMouse().getY() > 2*tileY + 26)))) && field[0][2] == 0){
				
					field[0][2] = 1;
					turn = false;
				}else if((game.getKeyboard().down || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 2*tileX && game.getMouse().getX() > 1*tileX) && (game.getMouse().getY() < 3*tileY + 26 && game.getMouse().getY() > 2*tileY + 26)))) && field[1][2] == 0){
				
					field[1][2] = 1;
					turn = false;
				}else if((game.getKeyboard().downright || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 3*tileX && game.getMouse().getX() > 2*tileX) && (game.getMouse().getY() < 3*tileY + 26 && game.getMouse().getY() > 2*tileY + 26)))) && field[2][2] == 0){
				
					field[2][2] = 1;
					turn = false;
					
				}
				
				{
					for(int i = 0; i <= 2; i++){
					
					for(int j = 0; j <= 2; j++){
					
						if(field[i][j] == 0){
							
							checkField += 1;
						}
							
						if((field[i][0] == 1 && field[i][1] == 1  && field[i][2] == 1) || (field[i][0] == 2 && field[i][1] == 2  && field[i][2] == 2)){
								
							field[i][0] = 4;
							field[i][1] = 4;
							field[i][2] = 4;
								
							win = true;
						}else if((field[0][j] == 1 && field[1][j] == 1  && field[2][j] == 1) || (field[0][j] == 2 && field[1][j] == 2  && field[2][j] == 2)){
								
							field[0][j] = 6;
							field[1][j] = 6;
							field[2][j] = 6;
								
							win = true;
						}else if((field[0][0] == 1 && field [1][1] == 1 && field[2][2] == 1) || (field[0][0] == 2 && field [1][1] == 2 && field[2][2] == 2)){
								
							field[0][0] = 3;
							field[1][1] = 3;
							field[2][2] = 3;
								
							win = true;
						}else if((field[2][0] == 1 && field [1][1] == 1 && field[0][2] == 1) || (field[2][0] == 2 && field [1][1] == 2 && field[0][2] == 2)){
								
							field[2][0] = 5;
							field[1][1] = 5;
							field[0][2] = 5;
								
							win = true;
						}
					
					}
				
				}if((checkField == 0) && !win){
					
					full = true;
					win = true;	
					
				}checkField = 0;
				
				}
		
			}else if(!turn && Bot.getBot() == null){
			
				if((game.getKeyboard().upleft || (game.getMouse().isLeftPressed() && (game.getMouse().getX() < tileX && game.getMouse().getY() < tileY + 26 && game.getMouse().getY() > 26))) && field[0][0] == 0){
					
					field[0][0] = 2;
					turn = true;
				}else if((game.getKeyboard().up || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 2*tileX && game.getMouse().getX() > tileX) && game.getMouse().getY() < tileY + 26))) && game.getMouse().getY() > 26 && field[1][0] == 0){
				
					field[1][0] = 2;
					turn = true;
				}else if((game.getKeyboard().upright || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 3*tileX && game.getMouse().getX() > 2*tileX) && game.getMouse().getY() < tileY + 26))) && game.getMouse().getY() > 26 && field[2][0] == 0){
				
					field[2][0] = 2;
					turn = true;
				}else if((game.getKeyboard().left || (game.getMouse().isLeftPressed() && (game.getMouse().getX() < tileX && (game.getMouse().getY() < 2*tileY + 26 && game.getMouse().getY() > tileY + 26)))) && field[0][1] == 0){
				
					field[0][1] = 2;
					turn = true;
				}else if((game.getKeyboard().centre || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 2*tileX && game.getMouse().getX() > tileX) && (game.getMouse().getY() < 2*tileY + 26 && game.getMouse().getY() > tileY + 26)))) && field[1][1] == 0){
				
					field[1][1] = 2;
					turn = true;
				}else if((game.getKeyboard().right || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 3*tileX && game.getMouse().getX() > 2*tileX) && (game.getMouse().getY() < 2*tileY + 26 && game.getMouse().getY() > tileY + 26)))) && field[2][1] == 0){
				
					field[2][1] = 2;
					turn = true;
				}else if((game.getKeyboard().downleft || (game.getMouse().isLeftPressed() && (game.getMouse().getX() < tileX && (game.getMouse().getY() < 3*tileY + 26 && game.getMouse().getY() > 2*tileY + 26)))) && field[0][2] == 0){
				
					field[0][2] = 2;
					turn = true;
				}else if((game.getKeyboard().down || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 2*tileX && game.getMouse().getX() > 1*tileX) && (game.getMouse().getY() < 3*tileY + 26 && game.getMouse().getY() > 2*tileY + 26)))) && field[1][2] == 0){
				
					field[1][2] = 2;
					turn = true;
				}else if((game.getKeyboard().downright || (game.getMouse().isLeftPressed() && ((game.getMouse().getX() < 3*tileX && game.getMouse().getX() > 2*tileX) && (game.getMouse().getY() < 3*tileY + 26 && game.getMouse().getY() > 2*tileY + 26)))) && field[2][2] == 0){
				
					field[2][2] = 2;
					turn = true;
				}
			
			}else if(!turn && Bot.getBot() == game.easyBot){
				
				switch(Bot.getBot().turn(field)){
					
					case 1:
						field[0][0] = 2;
						turn = true;
						break;
						
					case 2:
						field[1][0] = 2;
						turn = true;
						break;
						
					case 3:
						field[2][0] = 2;
						turn = true;
						break;
						
					case 4:
						field[0][1] = 2;
						turn = true;
						break;
						
					case 5:
						field[1][1] = 2;
						turn = true;
						break;
						
					case 6:
						field[2][1] = 2;
						turn = true;
						break;
						
					case 7:
						field[0][2] = 2;
						turn = true;
						break;
						
					case 8:
						field[1][2] = 2;
						turn = true;
						break;
						
					case 9:
						field[2][2] = 2;
						turn = true;
						break;
						
				}
				
			}
		
		}
		
	}

	@Override
	public void render(Graphics G) {
		
		if(turn){
			
			G.drawImage(Assets.Xturn, 0, 0, null);
			G.drawImage(Assets.Xturn, tileX, 0, null);
			G.drawImage(Assets.Xturn, tileX*2, 0, null);
		}else{
			
			G.drawImage(Assets.Oturn, 0, 0, null);
			G.drawImage(Assets.Oturn, tileX, 0, null);
			G.drawImage(Assets.Oturn, tileX*2, 0, null);
		}
		
		if(win){
			
			if(full) {
				
				G.drawImage(Assets.draw, 0, 0, null);
				G.drawImage(Assets.restart, tileX, 0, null);
				G.drawImage(Assets.menu, tileX*2, 0, null);
			}else if(turn){
				
				G.drawImage(Assets.Owon, 0, 0, null);
				G.drawImage(Assets.restart, tileX, 0, null);
				G.drawImage(Assets.menu, tileX*2, 0, null);
			}else{
				
				G.drawImage(Assets.Xwon, 0, 0, null);
				G.drawImage(Assets.restart, tileX, 0, null);
				G.drawImage(Assets.menu, tileX*2, 0, null);
			}
			
		}
		
		for(int i = 0; i <= 2; i++){
			for(int j = 0; j <= 2; j++){
				
				if(field[i][j] == 0){
					
					G.drawImage(Assets.Null, tileX*i, 26 + tileY*j, null);
				} else if(field[i][j] == 1){
					
					G.drawImage(Assets.X, tileX*i, 26 + tileY*j, null);
				} else if(field[i][j] == 2){
					
					G.drawImage(Assets.O, tileX*i, 26 + tileY*j, null);
				} else if(field[i][j] == 3){
					
					if(turn){
						
						G.drawImage(Assets.O, tileX*i, 26 + tileY*j, null);
					}else{
						
						G.drawImage(Assets.X, tileX*i, 26 + tileY*j, null);
					}
					
					G.drawImage(Assets.cross1, tileX*i, 26 + tileY*j, null);
				} else if(field[i][j] == 4){
					
					if(turn){
						
						G.drawImage(Assets.O, tileX*i, 26 + tileY*j, null);
					}else{
						
						G.drawImage(Assets.X, tileX*i, 26 + tileY*j, null);
					}
					
					G.drawImage(Assets.cross2, tileX*i, 26 + tileY*j, null);
				} else if(field[i][j] == 5){
					
					if(turn){
						
						G.drawImage(Assets.O, tileX*i, 26 + tileY*j, null);
					}else{
						
						G.drawImage(Assets.X, tileX*i, 26 + tileY*j, null);
					}
					
					G.drawImage(Assets.cross3, tileX*i, 26 + tileY*j, null);
				} else if(field[i][j] == 6){
					
					if(turn){
						
						G.drawImage(Assets.O, tileX*i, 26 + tileY*j, null);
					}else{
						
						G.drawImage(Assets.X, tileX*i, 26 + tileY*j, null);
					}
					
					G.drawImage(Assets.cross4, tileX*i, 26 + tileY*j, null);
				}
				
			}
			
		}
		
	}
	
}
